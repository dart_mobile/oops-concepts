class Attack {
  void shootable() {
    print("pew pew!!!");
  }

  void slashable() {
    print("chab chab!!!");
  }
}

class Movement {
  void flyable() {
    print("phab phab!!!");
  }

  void walkable() {
    print("tok-taek tok-taek!!!");
  }
}

class Skill {
  void makeDamage() {
    print("make a hit!!!");
  }
}

class Print {
  String? name;
  Print(String name) {
    this.name = name;
  }
  void printName() {
    print("My name is " + name.toString());
  }
}

class WalkCharacters extends Print with Attack, Movement, Skill {
  String? name;
  String? atk;
  String? skill;

  WalkCharacters(String name, String atk, String skill) : super('') {
    this.name = name;
    this.atk = atk;
    this.skill = skill;
  }

  @override
  void walkable() {
    print("walk with his feet");
    super.walkable();
  }
}

class FlyCharacters extends Print with Attack, Movement, Skill {
  String? name;
  String? atk;
  String? skill;
  FlyCharacters(String name, String atk, String skill) : super('') {
    this.name = name;
    this.atk = atk;
    this.skill = skill;
  }

  @override
  void flyable() {
    print("fly with her wings");
    super.flyable();
  }
}

main(List<String> args) {
  WalkCharacters player1 = new WalkCharacters("Zephys", "spear", "pierce");
  player1.printName();
  player1.walkable();
  print("---------------------");
  FlyCharacters player2 = new FlyCharacters("Maloch", "ax", "slash");
  player2.printName();
  player2.flyable();
}
