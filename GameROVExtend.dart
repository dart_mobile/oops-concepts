class GameCharacter {
  String? name;
  String? classPlayer;
  int? power;

  GameCharacter(String name, String classPlayer, int power) {
    this.name = name;
    this.classPlayer = classPlayer;
    this.power = power;
  }
  void walk() {
    print("Character " + name.toString() + " walk.");
  }

  void attack() {
    print("Character " +
        name.toString() +
        " attack with " +
        classPlayer.toString() +
        " make " +
        power.toString() +
        " damage.");
  }

  void skill() {
    print("Make a hit!!!");
  }

  void selectFirstSkill() {
    print("\nselect First Skill");
    print("Make a hit!!!");
  }

  void selectSecondSkill() {
    print("\nselect Second Skill");
    print("Make a hit!!!");
  }

  void selectUltimateSkill() {
    print("\nselect Ultimate Skill");
    print("Make a hit!!!");
  }

  String? getName() {
    return name;
  }

  String? getclassPlayer() {
    return classPlayer;
  }

  int? getpower() {
    return power;
  }
}

class Valhelin extends GameCharacter {
  Valhelin(super.name, super.classPlayer, super.power);

  @override
  void attack() {
    print("Character " +
        name.toString() +
        " attack with " +
        classPlayer.toString() +
        " make " +
        power.toString() +
        " damage.");
  }

  @override
  void skill() {
    print("First Skill : Bloody Hunt");
    print("Second Skill : Curse of Death");
    print("Ultimate Skill : Bullet Strom");
  }

  @override
  void selectFirstSkill() {
    print("\nselect First Skill");
    print("Use : Bloody Hunt");
    print("Damage done : 90");
    print("Cool down : 7 sec");
  }

  @override
  void selectSecondSkill() {
    print("\nselect Second Skill");
    print("Use : Curse of Death");
    print("Damage done : 250");
    print("Cool down : 7 sec");
  }

  @override
  void selectUltimateSkill() {
    print("\nselect Ultimate Skill");
    print("Use : Bullet Strom");
    print("Damage done : 300");
    print("Cool down : 16 sec");
  }
}

class Moren extends GameCharacter {
  Moren(super.name, super.classPlayer, super.power);

  @override
  void attack() {
    print("Character " +
        name.toString() +
        " attack with " +
        classPlayer.toString() +
        " make " +
        power.toString() +
        " damage.");
  }

  @override
  void skill() {
    print("First Skill : Tactical Maneuver");
    print("Second Skill : Impact Barrage");
    print("Ultimate Skill : Manetic Strom");
  }

  @override
  void selectFirstSkill() {
    print("\nselect First Skill");
    print("Use : Tactical Maneuver");
    print("Damage done : 90");
    print("Cool down : 8 sec");
  }

  @override
  void selectSecondSkill() {
    print("\nselect Second Skill");
    print("Use : Impact Barrage");
    print("Damage done : 400");
    print("Cool down : 8.5 sec");
  }

  @override
  void selectUltimateSkill() {
    print("\nselect Ultimate Skill");
    print("Use : Manetic Strom");
    print("Damage done : 400");
    print("Cool down : 30 sec");
  }
}

main(List<String> args) {
  print("---------------------");
  GameCharacter player = new GameCharacter("CherTam_0960", "normal", 50);
  player.walk();
  player.skill();
  player.attack();
  player.selectFirstSkill();
  player.selectSecondSkill();
  player.selectUltimateSkill();
  print("---------------------");
  Valhelin van = new Valhelin("van", "Marksman", 200);
  van.walk();
  van.skill();
  van.attack();
  van.selectFirstSkill();
  van.selectSecondSkill();
  van.selectUltimateSkill();
  print("---------------------");
  Moren moren = new Moren("moren", "Marksman", 200);
  moren.walk();
  moren.skill();
  moren.attack();
  moren.selectFirstSkill();
  moren.selectSecondSkill();
  moren.selectUltimateSkill();
  print("---------------------");
}
