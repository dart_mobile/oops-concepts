class Tank {
  String? name;
  Tank(String name) {
    this.name = name;
  }
  void speak() {
    print("I'm have much HP, I'm a Tank");
  }
}

class Warriors {
  String? name;
  Warriors(String name) {
    this.name = name;
  }
  void speak() {
    print("I'm have much Damage, I'm a Warriors");
  }
}

class Assassin {
  String? name;
  Assassin(String name) {
    this.name = name;
  }
  void speak() {
    print("I'm have much Movement, I'm a Assassin");
  }
}

class Mage {
  String? name;
  Mage(String name) {
    this.name = name;
  }
  void speak() {
    print("I'm have much Damage, I'm a Mage");
  }
}

class Markmans {
  String? name;
  Markmans(String name) {
    this.name = name;
  }
  void speak() {
    print("I'm have much Damage, I'm a Markmans");
  }
}

class Support {
  String? name;
  Support(String name) {
    this.name = name;
  }
  void speak() {
    print("I'm have much Heal blood, I'm a Support");
  }
}

class Grakk implements Support {
  String? name;
  Grakk(String name) {
    this.name = name;
  }

  @override
  void speak() {
    print("I'm " + name.toString() + " have much Heal blood, I'm a Support");
    print("and I have Magic to make a Damage");
  }
}

class Superman implements Warriors {
  String? name;
  Superman(String name) {
    this.name = name;
  }

  @override
  void speak() {
    print("I'm " + name.toString() + " have much Damage, I'm a Warriors");
    print("and I have Laser to make a Damage");
  }
}

class Raze implements Assassin {
  String? name;
  Raze(String name) {
    this.name = name;
  }

  @override
  void speak() {
    print("I'm " + name.toString() + " have much Damage, I'm a Assassin");
    print("and I have a Punch to make a Damage");
  }
}
class Lauriel implements Mage {
  String? name;
  Lauriel(String name) {
    this.name = name;
  }

  @override
  void speak() {
    print("I'm " + name.toString() + " have much Damage, I'm a Mage");
    print("and I have a Magic to make a Damage");
  }
}
main(List<String> args) {
  Grakk grakk = new Grakk("grakk");
  grakk.speak();
  Superman superman = new Superman("superman");
  superman.speak();
  Raze raze = new Raze("raze");
  raze.speak();
  Lauriel lauriel = new Lauriel("lauriel");
  lauriel.speak();
}
